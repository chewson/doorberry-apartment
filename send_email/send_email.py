from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import smtplib
import time
import os

class SendEmail(object):
    """
    Class to send emails wih picture
    """

    def __init__(self, user, password):
        self.emailuser =  user
        self.emailpwd = password

    def sendMail(self, to):
        """
        
        :param to: 
        :param subject: 
        :param text: 
        :param files: 
        :return: 
        """
        files = ['image.jpg']
        assert type(to[0]) == list

        ring_time = time.strftime("%H:%M:%S")
        ring_date = time.strftime("%d/%m/%Y")

        msg = MIMEMultipart()
        msg['From'] = self.emailuser
        msg['To'] = COMMASPACE.join(to[0])
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = 'Somebody is Ringing the Bell'

        msg.attach(MIMEText('Somebody has rung the doorbell at {0} and {1}'.format(ring_time, ring_date)))

        for file in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(file, "rb").read())
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"'
                            % os.path.basename(file))
            msg.attach(part)

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo_or_helo_if_needed()
        server.starttls()
        server.ehlo_or_helo_if_needed()
        server.login(self.emailuser, self.emailpwd)
        server.sendmail(self.emailuser, to[0], msg.as_string())
        server.quit()
        return
