import yaml

class ConfigYMLParser(object):

    @staticmethod
    def get_config(filename):
        with open(filename, "r") as input_stream:
            try:
                return yaml.load(input_stream)
            except yaml.YAMLError as e:
                print(e)