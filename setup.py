from setuptools import setup
import os.path

setup(
    name='DoorBerryPi',

    version='2017.05',

    packages=['sms','send_email','parse_input','config_file','call'],

    package_data={},

    url='',

    license='',

    author='chewson',

    author_email='chris.hewson@reservoirconcepts.com',

    description="DoorBerryPi - A python library to send sms/phone call/email to residents in a multiple complex apartment building",

    scripts=['doorbell'],

    install_requires=['twilio', 'playsound']
)
