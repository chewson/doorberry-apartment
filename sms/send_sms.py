from twilio.rest import Client
import time


class Sms(object):
    """
    Class to send SMS
    """

    def __init__(self, twilio):
        """
        
        """
        self.account_sid = twilio['SSID']
        self.auth_token = twilio['TOKEN']
        self.pinum = twilio['SMS_NUM']
        self.client = Client(self.account_sid , self.auth_token)

    def sendSms(self,number):
        """
        
        :param number: 
        :return: 
        """
        ring_time = time.strftime("%H:%M:%S")
        ring_date = time.strftime("%d/%m/%Y")
        self.client.api.account.messages.create(to=number,
                                            from_=self.pinum,
                                            body="Er is iemand aan de deur at {0} on {1}".format(ring_time, ring_date))