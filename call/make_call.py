from twilio.rest import Client
import time
from playsound import playsound


class PhoneCall(object):
    """
    Class to make phone calls
    """

    def __init__(self, twilio):
        """
        The constructor
        """
        # Get these credentials from http://twilio.com/user/account
        self.account_sid = twilio['SSID']
        self.auth_token = twilio['TOKEN']
        self.pinum = twilio['PHONE_NUM']
        self.url = twilio['PHONE_URL']
        self.client = Client(self.account_sid, self.auth_token)

    def makeCall(self, number_list):
        # Make the call
        cnt = 0
        if isinstance(number_list,list):
            number = number_list[cnt]
            cnt += 1
        else:
            number = number_list
            number_list = list()
        self.client.api.account.calls.create(to=number,
                                             from_=self.pinum,
                                             url=self.url)
        playsound('config_file/telephone-ring.wav')
        time.sleep(5)
        while True:
            status = self.client.calls.list()[0].status
            if status == 'completed':
                print('Your call has completed')
                break
            elif status == 'ringing':
                playsound('config_file/telephone-ring.wav')
                time.sleep(1)
            elif status == 'failed' and cnt<len(number_list):
                number = number_list[cnt]
                cnt += 1
                self.client.api.account.calls.create(to=number,
                                                     from_=self.pinum,
                                                     url=self.url)
            elif status == 'busy' and cnt<len(number_list):
                number = number_list[cnt]
                cnt += 1
                self.client.api.account.calls.create(to=number,
                                                     from_=self.pinum,
                                                     url=self.url)
            elif status == 'no-answer' and cnt<len(number_list):
                number = number_list[cnt]
                cnt += 1
                self.client.api.account.calls.create(to=number,
                                                     from_=self.pinum,
                                                     url=self.url)
            elif status == 'failed' or status == 'busy' or status == 'no-answer' and (cnt)==len(number_list):
                print('Your call has failed')
                break