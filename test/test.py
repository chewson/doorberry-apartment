#!/usr/bin/env python

import sys
from os.path import isfile
from parse_input import ConfigYMLParser
from send_email import SendEmail
from call import PhoneCall
from sms import Sms

if __name__ == '__main__':

    # Parse the the YAML input
    if len(sys.argv) < 2:
        sys.exit('Usage: doorbell doorbell_config')
    fname = sys.argv[1]
    if not isfile(fname):
        sys.exit('No such file "%s".' % fname)
    conf = ConfigYMLParser.get_config(fname)

    # Initiate SMS, phone and email
    sms_mess = Sms(conf['TWILIO_ACCOUNT'])
    phone_call = PhoneCall(conf['TWILIO_ACCOUNT'])
    send_email = SendEmail(conf['SEND_EMAIL_ADDRESS']['USERNAME'],
                           conf['SEND_EMAIL_ADDRESS']['PASSWORD'])

    # Instantiate the inputs for the raspberry pi
    for i in conf.keys():
        try:
            print('Pin number {} has been loaded into the raspberry pi'.format(conf[i]['PIN']))
        except:
            print('Key {} does not have an associated pin number'.format(i))

    while True:
        for i in conf.keys():
            try:
                if ((conf[i]['PIN']) == 21):
                    if conf[i]['PHONE'] is not None:
                        #sms_mess.sendSms(conf[i]['PHONE'])
                        phone_call.makeCall(conf[i]['PHONE'])
                    if conf[i]['EMAIL'] is not None:
                        send_email.sendMail([conf[i]['EMAIL']])
            except:
                continue
